" LINKS
" https://dougblack.io/words/a-good-vimrc.html

" Pathogen Plugin manager
execute pathogen#infect()
syntax on  " enable syntax and plugins (for netrw)
call pathogen#helptags()
filetype plugin indent on

let mapleader = "," " set leader key from default backslash to ,
set nocompatible " Use Vim settings, rather than Vi settings, enter the current millenium
set autoread " Read file when modified outside Vim
set noswapfile

" Show ruler and command visual aid
set ruler
set showcmd

" Disable bells
set noerrorbells
set visualbell
set t_vb=

set showmatch  " highlight matching bracets [{()}]
set cursorline " Highlight the cursor line
set splitright " Set default vertical split to right
set splitbelow
set number " Show line number and listchars
set relativenumber "Show relative numbering of line from the current line
nnoremap <Leader>l :set invnumber<CR>:set invrelativenumber<CR> 

" BUFFERS  TODO: add this plugin https://github.com/tpope/vim-unimpaired
nnoremap <silent> [b :bprevious<CR>
nnoremap <silent> ]b :bnext<CR>
nnoremap <silent> [B :bfirst<CR>
nnoremap <silent> ]B :blast<CR>
nnoremap <Leader>b :buffers<CR>:buffer<Space> 

" CDC = Change to Directory of Current file
command CDC cd %:p:h

" FINDING FILES:
set path+=** " Search down into subfolders and provides tab-completion for all file-related tasks
set wildmenu " Display all matching files when we tab complete
" NOW WE CAN:
" - Hit tab to :find by partial match
" - Use * to make it fuzzy
" - :b lets you autocomplete any open buffer

" TAG JUMPING:
" Create the `tags` file (may need to install ctags first)
command! MakeTags !ctags -R . 
" NOW WE CAN:
" - Use ^] to jump to tag under cursor
" - Use g^] for ambiguous tags
" - Use ^t to jump back up the tag stack
" THINGS TO CONSIDER:
" - This doesn't help if you want a visual list of tags

" SEARCHING:
set incsearch  " search as characters are entered
set hlsearch            " highlight matches
nnoremap <leader><space> :nohlsearch<CR> " turn off search highlight with ",<space>"


" FOLDING
set foldenable          " enable folding
set foldlevelstart=10   " open most folds by default
set foldnestmax=10      " 10 nested fold max
" space open/closes folds
nnoremap <space> za
set foldmethod=indent   " fold based on indent level

" move vertically by visual line, if there is long line across two lines it will not jump to the next line but the next part of long line
nnoremap j gj
nnoremap k gk

" highlight last inserted text
nnoremap gV `[v`]

" save session - After saving a assortment of windows you can reopen it with vim -S
nnoremap <leader>s :mksession<CR>


" AUTOCOMPLETE:
" The good stuff is documented in |ins-completion|
" HIGHLIGHTS:
" - ^x^n for JUST this file
" - ^x^f for filenames (works with our path trick!)
" - ^x^] for tags only
" - ^n for anything specified by the 'complete' option
" NOW WE CAN:
" - Use ^n and ^p to go back and forth in the suggestion list

" FILE BROWSING:
" Tweaks for browsing
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
" let g:netrw_list_hide=netrw_gitignore#Hide()
" let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
" NOW WE CAN:
" - :edit a folder to open a file browser
" - <CR>/v/t to open in an h-split/v-split/tab
" - check |netrw-browse-maps| for more mappings

" SNIPPETS:
" Read an empty HTML template and move cursor to title
"nnoremap ,html :-1read $HOME/.vim/.skeleton.html<CR>3jwf>a

" Spaces & Tabs
set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set expandtab       " tabs are spaces

" COLOR SCHEME

let g:solarized_termcolors=256
" colorscheme codedark
"set background=light   <<  this is for solarized scheme
set background=dark
colorscheme solarized


" noremap Q !!$SHELL<CR> " execute the commands on the line in the shell and read the output

" smooth scrolling effect CTRL-D CTRL-U
:map <C-U> <C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y>
:map <C-D> <C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E>

" VIM-AIRLINE plugin settings
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme = 'solarized'

" IndentLine plugin
let g:indentLine_setConceal = 1
let g:indentLine_char = '┊'
let g:indentLine_color_term = 239
let g:indentLine_fileType = ['yaml', 'yml']

" NERDTree plugin settings:
nnoremap <leader>n :NERDTreeToggle<CR>

" PYTHON 
" Flag unusued whitespace
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
" PEP8 identation
au BufNewFile,BufRead *.py
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4
    \ set textwidth=79
    \ set expandtab
    \ set autoindent
    \ set fileformat=unix

" Settings for Fullstack development
au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2


" Yaml settings
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

